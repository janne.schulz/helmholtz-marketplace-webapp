/* eslint class-methods-use-this: ["error", { "exceptMethods": ["_goToServices"] }] */
import { LitElement, html, css } from 'lit-element';

import '@material/mwc-button';

import './sections/mission-section.js';
import './sections/feature-section.js';
import '../../common-components/notification-banner/notification-banner.js';
import '../../common-components/header/helmholtz-cloud-header.js';
import '../../common-components/footer/helmholtz-cloud-footer.js';

class LandingView extends LitElement
{
    constructor()
    {
        super();
        this.addEventListener('wheel', this._scrollListener, true);
        this.addEventListener(
            'helmholtz-cloud-dismiss-notification-landing',
            this._notificationDismissalListener, true);
    }

    static get styles()
    {
        return css`
            :host {
                width: 100%;
                height: 100%;
                min-width: 320px;
            }
            header,
            main,
            footer {
                width: 100%;
                min-width: 320px;
            }
            header {
                position: fixed;
                top: 0;
                z-index: 100;
            }
            .header-shadow {
                box-sizing: border-box;
                box-shadow: 0px 0px 20px rgb(0 0 0 / 50%);
                border-bottom: 1px solid #c5c5c5;
            }
            main {
                display: flex;
                flex-direction: column;
                padding-top: 50px; /*must be equal to the height of the header*/
            }
            .hero {
                height: 100vh;
                min-height: 345px;
                background-color: #005aa0;
                color: #fff;
                display: flex;
                flex-direction: column;
                place-content: center;
                align-items: center;
                text-align: center;
                padding: 5%;
                box-sizing: border-box;
                background-attachment: fixed;
                background-position: center;
                background-repeat: no-repeat;
                background-size: cover;
                background-image: url('./media/i/blue-background_16_9.jpg');
            }
            .hero h1 {
                font-size: 4em;
                margin: 0;
                font-family: 'Hermann';
            }
            .hero mwc-button {
                --mdc-theme-primary: #8cb422;
                --mdc-theme-on-primary: white;
                width: 200px;
                transform: skew(-20deg);
                border-radius: 4px;
            }
            .definition {
                font-size: 1.2em;
                font-weight: 300;
            }
            .hide {
                display: none;
            }
            .arrow {
                position: absolute;
                top: 90%;
                left: 50%;
                transform: translate(-50%, -50%);
            }
            .arrow span {
                display: block;
                width: 30px;
                height: 30px;
                border-bottom: 5px solid #dedede;
                border-right: 5px solid #dedede;
                transform: rotate(45deg);
                margin: -10px;
                animation: animate 2s infinite;
            }
            .arrow span:nth-child(2) {
                animation-delay: -0.2s;
            }
            .arrow span:nth-child(3) {
                animation-delay: -0.4s;
            }
            @keyframes animate {
                0% {
                    opacity: 0;
                    transform: rotate(45deg) translate(-20px, -20px);
                }
                50% {
                    opacity: 1;
                }
                100% {
                    opacity: 0;
                    transform: rotate(45deg) translate(20px, 20px);
                }
            }
            notification-banner {
                background-color: #e3e3e3;
            }
            /* mission */
            .mission {
                background-color: #e4e4e4;
                width: 100%;
            }

            /* explore services */
            .explore {
                color: #5a5a5a;
                background-color: white;
                background-image: repeating-linear-gradient(45deg, white, transparent 500px);
                background-attachment: fixed;
            }
            @media screen and (min-width: 700px) {
                .hero {
                    height: 95vh;
                }
                .hero p {
                    font-size: 1.2em;
                    line-height: 1.5em;
                }
            }
        `;
    }

    render()
    {
        const notification = window.localStorage.getItem('notification-landing');
        return html`
            <header class="header-shadow">
                <helmholtz-cloud-header></helmholtz-cloud-header>
                ${notification !== "dismiss" ? html`<notification-banner></notification-banner>` : ``}
            </header>
            <main>
                <section class="hero">
                    <div class="inner-section">
                        <h1>Helmholtz Cloud</h1>
                        <div class="definition">
                            <div>Helmholtz Cloud supports scientific work from all Helmholtz research domains and centres.</div>
                            <div>Browse our cloud services and enjoy seamless access with single-sign on.</div>
                        </div>
                        <p>Note: Helmholtz Cloud is currently in pilot phase and will go in production during 2021.</p>
                        <div>
                            <div @click="${this._goToServices}">
                                <mwc-button raised>Show Services</mwc-button>
                            </div>
                        </div>
                    </div>
                    <div class="arrow">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </section>
                <section class="explore">
                    <feature-section></feature-section>
                </section>
                <section class="mission">
                    <mission-section></mission-section>
                </section>
            </main>
            <footer>
                <helmholtz-cloud-footer backgroundcolor></helmholtz-cloud-footer>
            </footer>
        `;
    }

    _goToServices()
    {
        window.location.href = `/#/services`;
    }

    _scrollListener()
    {
        this.removeEventListener('wheel', this._scrollListener, true);
        this.shadowRoot.querySelector('.arrow').classList.add('hide');
    }

    _notificationDismissalListener()
    {
        this.shadowRoot.querySelector('notification-banner').classList.add('hide');
    }
}
customElements.define('landing-view', LandingView);
