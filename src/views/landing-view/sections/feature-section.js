/* eslint no-param-reassign: ["error", { "props": false }] */
/* eslint class-methods-use-this: ["error", { "exceptMethods": ["_scroll"] }] */
import { LitElement, html, css } from 'lit-element';

import '@material/mwc-fab';

class FeatureSection extends LitElement
{
    constructor()
    {
        super();
        this.services = [
            {
                gitlab: {
                    description: 'GitLab is a web-based DevOps lifecycle tool that provides ' +
                        'a Git-repository manager providing wiki, issue-tracking and ' +
                        'continuous integration and deployment pipeline features, ' +
                        'using an open-source license, developed by GitLab Inc.',

                    summary: 'a complete DevOps platform, delivered as a single application'
                }
            },
            {
                openstack: {
                    description: 'OpenStack is a free open standard cloud computing platform, ' +
                        'mostly deployed as infrastructure-as-a-service (IaaS) in both public ' +
                        'and private clouds where virtual servers and other resources are made ' +
                        'available to users.',

                    summary: 'free open standard cloud computing platform'
                }
            },
            {
                mattermost: {
                    description: 'Mattermost is an open-source, self-host-able online chat service ' +
                        'with file sharing, search, and integrations.',

                    summary: 'chat service with file sharing, search, and integrations'
                }
            },
            {
                zammad: {
                    description: 'Zammad is a free help desk or issue tracking system. It offers the ' +
                        'connection of various channels such as e-mail , chat , telephone , ' +
                        'Twitter or Facebook.',

                    summary: 'web based ticketing system for helpdesk or customer support'
                }
            },
            {
                jupyterhub: [
                    'JupyterHub is a multi-user server for Jupyter Notebooks. ' +
                    'It is designed to support many users by spawning, managing, and ' +
                    'proxying many singular Jupyter Notebook servers.',

                    'multi-user server for Jupyter Notebooks'
                ]
            },
            {
                nextcloud: [
                    'Nextcloud is a suite of client-server software for creating ' +
                    'and using file hosting services.',

                    'on-site file share and collaboration platform'
                ]
            },
        ];
    }

    static get properties()
    {
        return {
            services: {
                type: Array,
            },
        };
    }

    static get styles()
    {
        return css`
            :host {
                display: block;
                overflow: hidden;
                -ms-overflow-style: none;
                scrollbar-width: none;
            }
            :host::-webkit-scrollbar {
                display: none;
            }
            * {
                box-sizing: border-box;
            }
            .title {
                text-align: center;
                padding-top: 1.2em;
            }
            .title h2 {
                margin: 0;
                font-size: 2em;
            }
            .part-a {
                display: flex;
            }
            .part-a div {
                width: 50%;
                margin: -17px;
            }
            .end {
                justify-content: flex-end;
            }
            .h2-first {
                color: #636262;
            }
            .h2-last {
                color: orange;
            }
            .content {
                padding: 20px;
            }

            @media screen and (max-width: 699.9px) {
                mwc-fab {
                    display: none;
                }
                .inner-top {
                    width: 100%;
                    height: 100%;
                    padding: 140px 10px 10px 10px;
                    background-repeat: no-repeat;
                    background-position: top center;
                    background-size: 140px;
                }
                .card:nth-of-type(7) .inner-top {
                    padding: 80px 10px 10px 10px;
                    background-size: 180px;
                }
                .description {
                    order: 2;
                    display: block;
                    margin-block: 1em;
                    margin-inline: 0px;
                    font-size: 1.1em;
                    line-height: 2;
                }
                .inner-top h3 {
                    font-size: 1.7em;
                }
                .description div:last-of-type {
                    display: none;
                }
                .card {
                    margin: 14px;
                    padding: 24px;
                    border: 1px solid #ddd;
                    text-align: center;
                    display: flex;
                    flex-direction: column;
                }
                .h {
                    display: none;
                }
            }

            @media screen and (min-width: 700px) {
                .content {
                    display: flex;
                    height: 300px;
                    justify-content: center;
                }
                mwc-fab {
                    --mdc-theme-secondary: white;
                    --mdc-theme-on-secondary: black;
                    position: relative;
                    top: 40%;
                    z-index: 2;
                }
                mwc-fab:last-of-type {
                    left: -2%;
                }
                mwc-fab:first-of-type {
                    right: -2%;
                }
                .services-container {
                    flex: 1 1 auto;
                    max-width: calc(100vw - 68px);
                    -ms-overflow-style: none;
                    scrollbar-width: none;
                    white-space: nowrap;
                    overflow-y: scroll;
                    display: flex;
                    align-items: center;
                    transition: 450ms transform;
                }
                .services-container::-webkit-scrollbar {
                    display: none;
                }
                .card {
                    display: inline-block;
                    min-width: 280px;
                    height: 140px;
                    margin-right: 10px;
                    border: 1px solid #ececec;
                    background-color: #fff;
                    border-radius: 6px;
                    transition: 450ms all;
                    transform-origin: center left;
                    box-shadow: 0 1px 15px rgba(27, 31, 35, 0.15),
                        0 0 1px rgba(106, 115, 125, 0.35);
                }
                .card:first-of-type {
                    margin-left: 40px;
                }
                .top {
                    display: block;
                    width: 100%;
                    height: 100%;
                    padding-right: 10px;
                }
                .inner-top {
                    width: 100%;
                    height: 100%;
                    padding: 14px 50px 0 24px;
                    background-repeat: no-repeat;
                    background-position: right;
                    background-size: 46px;
                }
                .inner-top h3 {
                    margin: 0;
                    font-weight: 300;
                    font-size: 24px;
                    color: #24292e;
                    margin-bottom: 5px;
                }
                .description {
                    width: 100%;
                    height: 48px;
                    font-size: 0.8em;
                    font-family: monospace;
                }
                .description div {
                    width: 100%;
                    height: 100%;
                    white-space: normal;
                    padding-right: 10px;
                }
                .description div:first-of-type {
                    display: none;
                }
                .h {
                    min-width: 30px;
                    visibility: hidden;
                }
            }

            @media screen and (min-width: 1200px) {
                .services-container {
                    max-width: 1200px;
                }
            }
        `;
    }

    render()
    {
        return html`
            <div>
                <div class="title">
                    <h2 class="h2-first">Our Services cover many popular software solutions:</h2>
                </div>
                <div class="content">
                    <mwc-fab icon="navigate_before" @click="${this._scrollLeft}"></mwc-fab>
                    <div class="services-container">
                        ${this.services.map(
                            service => html`
                                <div class="card">
                                    <div class="top">
                                        <div class="inner-top" 
                                                style="background-image: url(media/feature-services-logos/${Object.keys(service)}.svg)">
                                            <h3>${Object.keys(service)}</h3>
                                            <div class="description">
                                                ${Object.values(Object.values(service)[0]).map(i => html`<div>${i}</div>`)}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            `
                        )}
                        <div class="h feature-card">q</div>
                    </div>
                    <mwc-fab icon="navigate_next" @click="${this._scrollRight}"></mwc-fab>
                </div>
            </div>
        `;
    }

    _scrollRight()
    {
        const el = this.shadowRoot.querySelector('.services-container');
        this._scroll(el, el.scrollLeft + 300, 600).then();
    }

    _scrollLeft()
    {
        const el = this.shadowRoot.querySelector('.services-container');
        this._scroll(el, el.scrollLeft - 300, 600).then();
    }

    _scroll(element, target, duration)
    {
        /* adopted https://coderwall.com/p/hujlhg/smooth-scrolling-without-jquery */
        const targetRoundUp = Math.round(target);
        const durationRoundUp = Math.round(duration);

        if (durationRoundUp < 0) {
            return Promise.reject(new Error('bad duration'));
        }
        if (durationRoundUp === 0) {
            element.scrollLeft = targetRoundUp;
            return Promise.resolve();
        }

        const startTime = Date.now();
        const endTime = startTime + durationRoundUp;

        const startLeft = element.scrollLeft;
        const distance = targetRoundUp - startLeft;

        const smoothStep = function (start, end, point) {
            if (point <= start) {
                return 0;
            }
            if (point >= end) {
                return 1;
            }
            const x = (point - start) / (end - start);
            return x * x * (3 - 2 * x);
        };

        return new Promise(function (resolve, reject) {
            let previousLeft = element.scrollLeft;
            const scrollFrame = function () {
                if (element.scrollLeft !== previousLeft) {
                    reject(new Error('interrupted'));
                    return;
                }
                const now = Date.now();
                const point = smoothStep(startTime, endTime, now);
                const frameLeft = Math.round(startLeft + distance * point);
                element.scrollLeft = frameLeft;

                if (now >= endTime) {
                    resolve();
                    return;
                }
                if (element.scrollLeft === previousLeft && element.scrollLeft !== frameLeft) {
                    resolve();
                    return;
                }
                previousLeft = element.scrollLeft;
                setTimeout(scrollFrame, 0);
            };
            setTimeout(scrollFrame, 0);
        });
    }
}
customElements.define('feature-section', FeatureSection);
