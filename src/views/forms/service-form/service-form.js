import {css, html, LitElement} from 'lit-element';

class ServiceForm extends LitElement
{
    constructor()
    {
        super();
        this.organizations = [];
        this.uuids = [];
        const headers = new Headers({
            "Accept": "application/json"
        });
        if (sessionStorage.getItem('auth_status')) {
            headers.append("Authorization", `Bearer ${sessionStorage.getItem("access_token")}`);
        }
        fetch(`${window.location.origin}/api/v0/services`, {headers: headers})
            .then((response) => {
                if (response.status !== 200) {
                    throw new Error(`Looks like there was a problem. Status Code: ${response.status}`);
                }
                return response.json();
            })
            .then(data => {
                this.services = data.content;

                this.services.forEach(service => {
                    this.uuids.push(service.uuid)
                });
            })
            .catch(e => console.log(e));

        fetch(`${window.location.origin}/api/v0/organizations`, {headers: headers})
            .then((response) => {
                if (response.status !== 200) {
                    throw new Error(`Looks like there was a problem. Status Code: ${response.status}`);
                }
                return response.json();
            })
            .then(data => {
                this.organizations = data.content;
            })
            .catch(e => console.log(e));
    }

    static get properties()
    {
        return {
            organizations: {
                type: Array
            },
            services: {
                type: Array
            },
            uuids: {
                type: Array
            },
            currentService: {
                type: Object
            },
            stillCurrentService: {
                type: Object
            }
        }
    }
    static get styles()
    {
        return css`
            :host {
                width: 100%;
                height: 100%;
                overflow: hidden;
            }
            header,
            main,
            footer {
                width: 100%;
            }
            header {
                position: fixed;
                top: 0;
                z-index: 100;
                box-sizing: border-box;
                border-bottom: 1px solid #274C69;
                box-shadow: 0 -2px 8px rgb(0 0 0 / 9%),
                    0 4px 8px rgb(0 0 0 / 6%),
                    0 1px 2px rgb(0 0 0 / 30%),
                    0 2px 6px rgb(0 0 0 / 15%)
            }
            * {
                box-sizing: border-box;
            }
            main {
                display: flex;
                padding-top: 50px; /*must be equal to the height of the header*/
                width: 100%;
                height: 100%;
                overflow: hidden;
            }
            .side-nav {
                width: 80px;
                height: 100%;
                background-color: #fff;
                /*background-color: #005aa0;*/
            }
            .container {
                width: calc(100% - 80px);
                background-color: #e8e8e8;
                padding: 10px;
            }
            /* css for the form-card and content*/
            .form-card {
                width: 700px;
                height: 100%;
                margin: auto;
            }
            .card-top {
                width: 100%;
                height: 100px;
                display: flex;
                justify-content: center;
                align-items: center;
            }
            .card-content {
                width: 100%;
                height: calc(100% - 100px);
                padding: 30px;
                overflow-y: scroll;
                background-color: #fff;
                border: 1px solid #cdcdce;
                border-radius: 8px;
            }
            mwc-select#uuids {
                width: 400px;
            }
            mwc-select, mwc-textfield, mwc-textarea, mwc-button {
                width: 100%;
            }
            mwc-textarea {
                height: 150px;
                /*max-height: 100px;*/
            }
            mwc-icon-button {
                display: none;
            }
            mwc-button {
                margin-top: 20px;
                --mdc-theme-primary: #8cb422;
                --mdc-theme-on-primary: white;
            }
            .row {
                border-bottom: 1px solid #cdcdcd;
                padding: 40px 0;
            }
            .editable {
                padding: 20px 0;
            }
            .show {
                display: inline-block;
                position: absolute;
                right: calc(50% - 350px);
                color: #005aa0;
                --mdc-icon-size: 20px;
            }
            .no-selection {
                text-align: center;
                width: 100%;
                height: 100%;
                display: block;
                padding-top: 40%;
            }
            .warning {
                color: red;
                font-size: 0.7em;
            }
        `;
    }
    render()
    {
        return html`
            <div class="form-card">
                <div class="card-top">
                    <mwc-select id="uuids" outlined label="service uuids" @action="${this._showService}">
                        <mwc-list-item></mwc-list-item>
                        ${this.uuids.map(uuid =>
                                html`<mwc-list-item value="${uuid}">${uuid}</mwc-list-item>`)}
                    </mwc-select>
                </div>
                <div class="card-content">
                    ${this.movingCurrentService ? html`
                                <div class="editable row"
                                        @mouseenter="${this._mouseEnter}" @mouseleave="${this._mouseLeave}">
                                    <mwc-textfield id="service-name" disabled required
                                            label="service name" @change="${this._saveServiceName}"
                                            value="${this.movingCurrentService.name}"></mwc-textfield>
                                    <mwc-icon-button icon="edit" @click="${this._edit}"></mwc-icon-button>
                                </div>
                                <div class="editable row"
                                        @mouseenter="${this._mouseEnter}" @mouseleave="${this._mouseLeave}">
                                    <mwc-textarea id="summary"
                                            disabled charCounter required maxlength="80" charCounter
                                            label="summary" @change="${this._saveSummary}"
                                            value="${this.movingCurrentService.summary ?
                            this.movingCurrentService.summary: ''}"></mwc-textarea>
                                    <mwc-icon-button icon="edit" @click="${this._edit}"></mwc-icon-button>
                                </div>
                                <div class="editable row"
                                        @mouseenter="${this._mouseEnter}" @mouseleave="${this._mouseLeave}">
                                    <mwc-textarea id="description"
                                            disabled charCounter required
                                            label="service description" @change="${this._saveDescription}"
                                            value="${this.movingCurrentService.description}"></mwc-textarea>
                                    <mwc-icon-button icon="edit" @click="${this._edit}"></mwc-icon-button>
                                </div>
                                <div class="editable row"
                                        @mouseenter="${this._mouseEnter}" @mouseleave="${this._mouseLeave}">
                                    <mwc-textfield disabled required id="url"
                                            label="entrypoint" @change="${this._saveUrl}"
                                            value="${this.movingCurrentService.url}"></mwc-textfield>
                                    <mwc-icon-button icon="edit" @click="${this._edit}"></mwc-icon-button>
                                </div>
                                <div class="row">
                                    <span> service providers</span>
                                    ${this.movingCurrentService["serviceProviders"] ?
                            html`
                                            ${this.movingCurrentService["serviceProviders"].map(provider =>
                                    html`
                                                    <div class="editable"
                                                            @mouseenter="${this._mouseEnter}" @mouseleave="${this._mouseLeave}">
                                                        <mwc-textfield id="software-name"
                                                                disabled required
                                                                label="software name" @change="${this._saveSoftwareName}"
                                                                value="${provider["serviceTechnicalName"]}"></mwc-textfield>
                                                        <mwc-icon-button icon="edit" @click="${this._edit}"></mwc-icon-button>
                                                    </div>

                                                    <mwc-select id="organization"
                                                            outlined label="organization" @change="${this._saveOrganisation}">
                                                        ${this.organizations.map(org =>
                                            html`
                                                                <mwc-list-item
                                                                    value="${org["abbreviation"]}"
                                                                    ?selected="${org["uuid"] === provider["organization"]["uuid"]}">
                                                                    ${org["abbreviation"]}</mwc-list-item>
                                                            `
                                    )}
                                                    </mwc-select>
                                                `
                            )}`:
                            html`
                                        <div><p class="warning">No provider is linked to this service yet</p></div>
                                        <div class="editable"
                                                @mouseenter="${this._mouseEnter}" @mouseleave="${this._mouseLeave}">
                                            <mwc-textfield id="software-name"
                                                disabled required
                                                label="software name" @change="${this._saveSoftwareName}"
                                                value=""></mwc-textfield>
                                            <mwc-icon-button icon="edit" @click="${this._edit}"></mwc-icon-button>
                                        </div>
                                        <mwc-select id="organization" outlined
                                                label="organization" @change="${this._saveOrganisation}">
                                            <mwc-list-item></mwc-list-item>
                                            ${this.organizations.map(org =>
                                    html`
                                                    <mwc-list-item
                                                        value="${org["abbreviation"]}">
                                                    ${org["abbreviation"]}</mwc-list-item>
                                                `)}
                                        </mwc-select>
                                    `}
                                </div>

                                <div>
                                    <mwc-button raised label="update" @click="${this._update}"></mwc-button>
                                </div>
                            `: html`
                                <span class="no-selection">service metadata will be display here</span>
                            `}
                </div>
            </div>
        `;
    }
    _update()
    {
        const headers = new Headers({
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Authorization": `Bearer ${sessionStorage.getItem("access_token")}`
        });
        const profile = this.stillCurrentService.url !== this.movingCurrentService.url ||
            this.stillCurrentService.name !== this.movingCurrentService.name ||
            this.stillCurrentService.description !== this.movingCurrentService.description;

        const movingProvider = this.movingCurrentService.serviceProviders[0];
        let provider;
        if (this.stillCurrentService.serviceProviders === null) {
            provider = movingProvider.organization["uuid"] && movingProvider.serviceTechnicalName;
        } else {
            const stillProvider = this.stillCurrentService.serviceProviders[0];
            provider = stillProvider.serviceTechnicalName !== movingProvider.serviceTechnicalName ||
                stillProvider.organization["uuid"] !== movingProvider.organization["uuid"];
        }

        console.log('profile', profile);
        console.log('provider', provider);
        if (sessionStorage.getItem('auth_status')) {
            if (profile && provider){
                this._updateServiceProfile(headers)
                    .then((r) => {
                        console.log('service profile is updated', r);
                        //change the value inside
                        this._updateServiceProvider(headers).then((r1)=>{
                            console.log('service provider is updated', r1);
                        })
                    }).catch(e => {console.log(e)});
            } else {
                if (profile)
                    this._updateServiceProfile(headers)
                        .then((r) => {
                            console.log('service profile is updated', r);
                        }).catch(e => {
                        console.log('error when updating a service profile', e)
                    });
                if (provider)
                    this._updateServiceProvider(headers)
                        .then((r) => {
                            console.log('service provider is updated', r);
                        }).catch(e => {
                        console.log('error when updating a provider', e)
                    });
            }
        } else {
            console.log('please login');
        }
    }
    _updateServiceProfile(headers)
    {
        const cloneService = JSON.parse(JSON.stringify(this.movingCurrentService));
        delete cloneService["serviceProviders"];
        delete cloneService["managementTeam"];
        console.log(cloneService);

        return fetch(`${window.location.origin}/api/v0/services/${this.movingCurrentService.uuid}`, {
            method: 'PUT',
            headers: headers,
            body: JSON.stringify(cloneService)
        }).then((response) => {
            if (response.status !== 200) {
                throw new Error(`Looks like there was a problem. Status Code: ${response.status}`);
            }
            resolve(response.json());
        }).catch(e => reject(e));

    }
    _updateServiceProvider(headers)
    {
        const cloneServiceProvider = JSON.parse(JSON.stringify(this.movingCurrentService.serviceProviders[0]));
        const cloneService = JSON.parse(JSON.stringify(this.movingCurrentService));
        delete cloneService["managementTeam"];
        cloneServiceProvider["marketService"] = cloneService;


        return fetch(`${window.location.origin}/api/v0/services/providers`, {
            method: 'PUT',
            headers: headers,
            body: JSON.stringify(cloneServiceProvider)
        }).then((response) => {
            if (response.status !== 200) {
                throw new Error(`Looks like there was a problem. Status Code: ${response.status}`);
            }
            resolve(response.json());
        }).catch(e => reject(e));
    }
    _edit(e)
    {
        e.stopPropagation();
        const editable = e.path.find(el => {
            return el.classList && el.classList.contains("editable");
        });

        editable.querySelector('mwc-textfield') ?
            editable.querySelector('mwc-textfield').disabled = false:
            editable.querySelector('mwc-textarea').disabled = false;
    }
    _showService()
    {
        const uuid = this.shadowRoot.querySelector('#uuids').selected.value;
        this.stillCurrentService = JSON.parse(JSON.stringify(this.services.find(service => service.uuid === uuid)));
        this.movingCurrentService = JSON.parse(JSON.stringify(this.services.find(service => service.uuid === uuid)));
    }
    _mouseEnter(e)
    {
        const editable = e.path.find(el => {
            return el.classList && el.classList.contains("editable");
        });
        editable.querySelector('mwc-icon-button').classList.add('show');
    }
    _mouseLeave(e)
    {
        const editable = e.path.find(el => {
            return el.classList && el.classList.contains("editable");
        });
        editable.querySelector('mwc-icon-button').classList.remove('show');
        editable.querySelector('mwc-textfield') ?
            editable.querySelector('mwc-textfield').disabled = true:
            editable.querySelector('mwc-textarea').disabled = true;
    }
    _saveServiceName()
    {
        this.movingCurrentService.name = this.shadowRoot.querySelector('#service-name').value;
    }
    _saveSummary()
    {
        this.movingCurrentService.summary = this.shadowRoot.querySelector('#summary').value
    }
    _saveDescription()
    {
        this.movingCurrentService.description = this.shadowRoot.querySelector('#description').value
    }
    _saveUrl()
    {
        this.movingCurrentService.url = this.shadowRoot.querySelector('#url').value
    }
    _saveSoftwareName()
    {
        const name = this.shadowRoot.querySelector('#software-name').value;
        if (this.movingCurrentService["serviceProviders"]) {
            this.movingCurrentService["serviceProviders"][0]["serviceTechnicalName"] = name;
        } else {
            this.movingCurrentService["serviceProviders"] = [{"serviceTechnicalName": name}];
        }
    }
    _saveOrganisation()
    {
        const organization = this.organizations.find(org => {
            return org["abbreviation"] === this.shadowRoot.querySelector('#organization').value
        });
        if (this.movingCurrentService["serviceProviders"]) {
            this.movingCurrentService["serviceProviders"][0]["organization"] = organization;
        } else {
            this.movingCurrentService["serviceProviders"] = [{"organization": organization}];
        }
    }
}
customElements.define('service-form', ServiceForm);