import { LitElement, html, css } from 'lit-element';

import '@material/mwc-icon-button';
import '@material/mwc-icon';

import '../../common-components/services-list/services-list.js';
import '../../common-components/service-profile/service-profile.js';
import '../../common-components/notification-banner/notification-banner.js';
import '../../common-components/header/helmholtz-cloud-header.js';
import '../../common-components/footer/helmholtz-cloud-footer.js';

class ServicesView extends LitElement
{
    constructor()
    {
        super();
        this.addEventListener('helmholtz-cloud-service-metadata', this._showDetailsPanel);
        this.addEventListener(
            'helmholtz-cloud-dismiss-notification-services',
            this._notificationDismissalListener, true);
        const headers = new Headers({
            "Accept": "application/json"
        });
        if (sessionStorage.getItem('auth_status')) {
            headers.append("Authorization", `Bearer ${sessionStorage.getItem("access_token")}`);
        }
        fetch(`${window.location.origin}/api/v0/services`, {headers: headers})
            .then((response) => {
                if (response.status !== 200) {
                    throw new Error(`Looks like there was a problem. Status Code: ${response.status}`);
                }
                return response.json();
            })
            .then(data => {
                this.services = data.content;
                this.dispatchEvent(
                    new CustomEvent('helmholtz-cloud-custom-element-status', {
                        detail: {customElementName: 'service-list'},
                        bubbles: true,
                        composed: true
                    }));
            })
            .catch(e => console.log(e));
        this.serviceContact = {
            "OpenStack (HDF Cloud)" : "ds-support@fz-juelich.de",
            "Mattermost" : "support@hifis.net",
            "HIFIS Helpdesk" : "support@hifis.net",
            "GitLab" : "support@hifis.net",
            "JupyterHub" : "ds-support@fz-juelich.de",
            "nubes" : "help@helmholtz-berlin.de",
            "bwSync&Share" : "servicedesk@scc.kit.edu",
            "B2Share" : "ds-support@fz-juelich.de"
        };
    }
    static get properties()
    {
        return {
            services: {
                type: Array
            },
            serviceName: {
                type: String
            },
            serviceContact: {
                type: Object
            }
        };
    }
    static get styles()
    {
        return css`
            :host {
                width: 100%;
                height: 100%;
                overflow: hidden;
            }
            header,
            main,
            footer {
                width: 100%;
            }
            header {
                position: fixed;
                top: 0;
                z-index: 100;
                box-sizing: border-box;
                box-shadow: 0 -2px 8px rgb(0 0 0 / 9%),
                    0 4px 8px rgb(0 0 0 / 6%),
                    0 1px 2px rgb(0 0 0 / 30%),
                    0 2px 6px rgb(0 0 0 / 15%)
            }
            .header-border {
                border-bottom: 1px solid #274C69;
            }
            * {
                box-sizing: border-box;
            }
            notification-banner {
                background-color: #e3e3e3;
            }
            main {
                display: flex;
                padding-top: 50px; /*must be equal to the height of the header*/
                width: 100%;
                height: 100vh;
                overflow: hidden;
                padding-bottom: 30px;
            }
            .list, .details {
                height: 100%;
            }
            .list {
                flex: 1 1 auto;
                padding: 20px;
                overflow-y: scroll;
                max-width: 100%;
            }
            .details {
                overflow: hidden;
                background-color: #efefef;
                width: 0;
                transition: width 0.3s linear;
            }
            #details.show {
                width: 600px;
            }
            #list.show {
                width: calc(100% - 600px);
            }
            .details .buttons {
                max-height: 50px;
                height: 5%;
                display: flex;
            }
            .buttons div {
                flex: 1 1 auto;
                display: flex;
                align-items: center;
                padding: 20px;
                color: #ff5722;
            }
            .buttons div a {
                text-decoration: none;
                font-size: 0.85em;
                padding-right: 5px;
                color: #ff5722;
                font-weight: 300;
            }
            .buttons div mwc-icon {
                font-size: 1.21em;
            }
            .details .details-content {
                background-color: white;
                margin: 20px;
                padding: 20px;
                border-radius: 3px;
                height: 90%;
                overflow-y: scroll;
            }
            footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
            }
            .hide {
                display: none;
            }
            @media screen and (max-width: 771px) {
                #list.show {
                    display: none;
                }
                #details.show {
                    width: 100vw;
                }
            }
            @media screen and (min-width: 772px) {
                #list.show {
                    width: calc(100% - 390px);
                }
                #details.show {
                    width: 390px;
                }
            }
            @media screen and (min-width: 1100px) {
                #list.show {
                    width: calc(100% - 600px);
                }
                #details.show {
                    width: 600px;
                }
            }
        `;
    }

    render()
    {
        const notification = window.localStorage.getItem('notification-services');
        const msg = "Disclaimer: All Service information provided here is for your information and convenience. " +
            "The respective service provider is responsible for providing the service and all required information " +
            "on usage conditions and regulations. <br>" +
            "<p><b>Not all services are accessible to all users.</b></p>";
        return html`
                ${notification === "dismiss" ?
                    html`
                        <header class="header-border">
                            <helmholtz-cloud-header backgroundcolor></helmholtz-cloud-header>
                        </header>`:
                    html`
                        <header>
                            <helmholtz-cloud-header backgroundcolor></helmholtz-cloud-header>
                            <notification-banner message=${msg} page="services"></notification-banner>
                        </header>`}
            </header>
            <main>
                <div id="list" class="list">
                    <services-list .services=${this.services}></services-card>
                </div>
                <div id="details" class="details">
                    <div class=buttons>
                        <div>
                            <a href="mailto:${this.serviceContact[`${this.serviceName}`]}">
                                need help with ${this.serviceName}</a><mwc-icon>help</mwc-icon>
                        </div>
                        <mwc-icon-button icon="close" @click="${this._closeDetailsPanel}"></mwc-icon-button>
                    </div>
                    <div id="panel" class="details-content"></div>
                </div>
            </main>
            <footer>
                <helmholtz-cloud-footer minima></helmholtz-cloud-footer>
            </footer>
        `;
    }
    _showDetailsPanel(e)
    {
        const el = this.shadowRoot.querySelector('#panel');
        const newService = e.detail.message;
        const oldService = el.querySelector('service-profile') ?
                el.querySelector('service-profile').service : null;
        if (!oldService || (newService["uuid"] !== oldService["uuid"])) {
            this.serviceName = newService['name'];
            const serviceProfile = document.createElement('service-profile');
            serviceProfile.uuid = newService["uuid"];
            serviceProfile.service = newService["service"];
            this._removeAllChildNodes(el);
            el.appendChild(serviceProfile);
        }
        this.shadowRoot.querySelector('#list').classList.add('show');
        this.shadowRoot.querySelector('#details').classList.add('show');
    }
    _closeDetailsPanel()
    {
        this.shadowRoot.querySelector('#list').classList.remove('show')
        this.shadowRoot.querySelector('#details').classList.remove('show')
    }
    _removeAllChildNodes(parent)
    {
        while (parent.firstChild) {
            parent.removeChild(parent.firstChild);
        }
    }
    _notificationDismissalListener()
    {
        this.shadowRoot.querySelector('notification-banner').classList.add('hide');
        this.shadowRoot.querySelector('header').classList.add('header-border');
    }
}
customElements.define('services-view', ServicesView);
