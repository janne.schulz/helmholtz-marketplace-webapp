/* eslint class-methods-use-this: ["error", { "exceptMethods": ["_logout", "_goToHomepage"] }] */
import { LitElement, html, css } from 'lit-element';

import '@material/mwc-icon-button';
import 'js-cookie';

class HelmholtzCloudHeader extends LitElement
{
    static get properties()
    {
        return {
            backgroundColor: {
                type: Boolean,
                attribute: true
            }
        };
    }

    static get styles()
    {
        return css`
            :host {
                display: flex;
                width: 100%;
                align-items: center;
                box-sizing: border-box;
                color: #005aa0;
                background-color: #fff;
                height: 50px;
            }
            :host([backgroundcolor]) {
                background-color: #005aa0;
                color: #fff;
            }
            .logo {
                display: flex;
                justify-content: center;
                flex: 1 1 auto;
            }
            .logo .project-name {
                display: none;
            }
            .logo img {
                height: 24px;
                width: 250px;
                cursor: pointer;
            }
            .top-nav {
                display: none;
            }
            
            @media screen and (min-width: 700px) {
                :host([backgroundcolor]) a {
                    color: #fff;
                }
                :host a {
                    color: #005aa0;
                }
                .menu {
                    display: none;
                }
                .top-nav {
                    display: flex;
                    height: 50px;
                    box-sizing: border-box;
                    font-weight: 300;
                }
                .top-nav a {
                    display: flex;
                    text-decoration: none;
                    padding: 1.5em;
                    align-items: center;
                }
                .logo {
                    display: block;
                    padding-left: 2%;
                }
                .logo img {
                    height: 24px;
                }
            }
        `;
    }

    render()
    {
        return html`
            <div class="menu">
                <mwc-icon-button
                    icon="menu"
                    slot="navigationIcon"
                    @click="${this._toggleDrawer}"></mwc-icon-button>
            </div>
            <div class="logo">
                <img @click="${this._goToHomepage}"
                     src=${this.backgroundColor ? 
                        `media/i/helmholtz-cloud_white_font.svg` : 
                        `media/i/helmholtz-cloud_blue_font.svg`}>
                <div class="project-name">Helmholtz Cloud</div>
            </div>
            <nav class="top-nav">
                <a target="_blank"
                    rel="noopener noreferrer"
                    href="https://hifis.net/team">Team</a>
                <a target="_blank"
                    rel="noopener noreferrer"
                    href="https://hifis.net/news">News</a>
                <a target="_blank"
                    rel="noopener noreferrer"
                    href="https://hifis.net/contact">Helpdesk</a>
                <a target="_blank"
                    rel="noopener noreferrer"
                    href="https://hifis.net/cloud-platform">About</a>
                ${sessionStorage.getItem('auth_status') === 'authenticated' ? 
                    html`<a @click=${this._logout}>Sign out</a>` : 
                    html`<a href="${window.location.origin}/oauth2/authorization/unity">Sign in</a>`
                }
            </nav>
        `;
    }

    _toggleDrawer()
    {
        this.dispatchEvent(
            new CustomEvent('helmholtz-main-menu-toggle', {
                bubbles: true,
                composed: true,
            })
        );
    }

    _logout()
    {
        fetch(`${window.location.origin}/logout`, {
            method: 'POST',
            headers: {
                'X-XSRF-TOKEN': window.Cookies.get('XSRF-TOKEN'),
            },
        })
            .then(response => {
                if (response.status !== 200) {
                    throw new Error(
                        `Looks like there was a problem. Status Code: ${response.status}`
                    );
                }
                sessionStorage.clear();
                if (window.location.hash === '' && window.location.path === '/') {
                    window.location.reload();
                } else {
                    window.location.replace('/');
                }
            })
            .catch(error => console.log(error));
    }

    _goToHomepage()
    {
        window.location.href = '/';
    }
}
customElements.define('helmholtz-cloud-header', HelmholtzCloudHeader);
