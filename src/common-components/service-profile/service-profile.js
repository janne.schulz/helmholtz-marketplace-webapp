import { LitElement, html, css } from 'lit-element';

class ServiceProfile extends LitElement
{
    constructor()
    {
        super();
        this.service = {};
        this.serviceProviders = [];
        this.software = "";
    }
    static get properties()
    {
        return {
            uuid : {type: String},
            service: {type: Object},
            serviceProviders: {type: Array},
            software: {type: String}
        };
    }
    static get styles()
    {
        return css`
            :host {
                width: 100%;
                height: 100%;
                padding: 20px;
            }
            .profile-container {
                display: flex;
                flex-direction: column;
            }
            .profile-container > h2 {
                color: #FF5722;
            }
            .description {
                width: 80%;
            }
            .description h4 {
                margin: 0;
                font-weight: 300;
            }
            .description p {
                margin-left: 10px;
                font-size: small;
            }
            .row {
                width: 100%;
                display: flex;
                border-bottom: 1px solid #eee;
                min-height: 40px;
                padding-top: 5px;
                align-items: center;
            }
            .row .title {
                min-width: 40%;
                max-width: 40%;
                font-weight: 250;
                font-size: small;
            }
            .column {
                flex-direction: column;
                align-items: start !important;
            }
            .column .title {
                padding-bottom: 10px;
            }
            .null {
                color: #E91E63;
                font-style: italic;
            }
            .sub-table {
                width: 90%;
            }
            .sub-table .row .title {
                color: green;
                min-width: 30%;
                max-width: 30%;
            }
            .column .sub-table {
                margin-left: 24px;
            }
            .null, .value {
                font-size: 0.9em;
                overflow-wrap: anywhere;
            }
        `;
    }

    connectedCallback()
    {
        super.connectedCallback();
        const headers = new Headers({
            "Accept": "application/json"
        });
        if (sessionStorage.getItem('auth_status')) {
            headers.append("Authorization", `Bearer ${sessionStorage.getItem("access_token")}`);
        }
        fetch(`${window.location.origin}/api/v0/services/${this.uuid}`, {headers:headers})
        .then((response) => {
                if (response.status !== 200) {
                    throw new Error(`Looks like there was a problem. Status Code: ${response.status}`);
                }
                return response.json();
            })
            .then(data => {
                this.service = data;
                this.dispatchEvent(
                    new CustomEvent('helmholtz-cloud-custom-element-status', {
                        detail: {customElementName: 'service-profile'},
                        bubbles: true,
                        composed: true})
                );
                return data;
            })
            .then(data => {
                var providers = [];
                data.serviceProviders.forEach(uuid => {
                    fetch(`${window.location.origin}/api/v0/organizations/${uuid}`, {headers: headers})
                    .then((response) => {
                        if (response.status !== 200) {
                            throw new Error(`Looks like there was a problem. Status Code: ${response.status}`);
                        }
                        return response.json();
                    })
                    .then(data => {
                        providers.push(data);
                        this.serviceProviders = providers;
                    })
                    .catch(e => {
                        console.log(e);
                    });
                });
                return data;
            })
            .then(data => {
                fetch(`${window.location.origin}/api/v0/software/${this.service.softwareList[0]}`, {headers: headers})
                .then((response) => {
                    if (response.status !== 200) {
                        throw new Error(`Looks like there was a problem. Status Code: ${response.status}`);
                    }
                    return response.json();
                })
                .then(data => {
                    this.software = data.name;
                })
                .catch(e => {
                    console.log(e);
                });
            })
            .catch(e => {
                console.log(e);
            });

    }
    render()
    {
        if (this.service == null) { return; }
        return html`
            <div class="profile-container">
                <h2>${this.service.name}</h2>
                <div class="description">
                    <h4>Description</h4>
                    <p>${this._returnString(this.service["description"])}</p>
                </div>
                <div class="row">
                    <span class="title">Software name</span>
                    <span class="value">${this.software}</span>
                </div>
                <div class="row">
                    <span class="title">Entrypoint</span>
                    <span class="value"><a href="${this.service.entryPoint}">${this.service.entryPoint}</a></span>
                </div>
                ${this.service["authentication"] != null ?
                    html`
                        <div class="row">
                            <span class="title">Authentication</span>
                            <span class="value">${this.service["authentication"]}</span>
                        </div>
                    `: ''
                }
                ${this.service["created"] != null ?
                    html`
                        <div class="row">
                            <span class="title">created</span>
                            <span class="value">${this.service["created"]}</span>
                        </div>
                    `: ''
                }
                ${this.service["created"] != null ?
                    html`
                        <div class="row">
                            <span class="title">lastModified</span>
                            <span class="value">${this.service['lastModified']}</span>
                        </div>
                    `: ''
                }
                ${this.service["lifecycleStatus"] != null ?
                    html`
                        <div class="row">
                            <span class="title">LifecycleStatus</span>
                            <span class="value">${this.service['lifecycleStatus']}</span>
                        </div>
                    `: ''
                }
                ${this.service["managementTeam"].length != 0 ?
                    html`
                        <div class="row">
                            <span class="title">Management Team</span>
                            <span class="value">${this.service['managementTeam'].join('')}</span>
                        </div>
                    `: ''
                }
                <div class="row column">
                    <span class="title">Host</span>
                    <div class="sub-table">
                        ${this.serviceProviders.map(provider =>
                            html`
                                <div class="row">
                                    <span class="title">name</span>
                                    <span class="value">${provider.name}</span>
                                </div>
                                <div class="row">
                                    <span class="title">abbreviation</span>
                                    <span class="value">
                                        ${provider.abbreviation === null ?
                                            'null': provider.abbreviation}
                                    </span>
                                </div>
                                <div class="row">
                                    <span class="title">url</span>
                                    <a class="value"
                                        href="${provider.url}">${provider.url}</a>
                                </div>
                            `
                        )}
                    </div>
                </div>
            </div>

        `;
    }

    _returnString(str)
    {
        return document.createRange().createContextualFragment(`${ str }`);
    }
}
customElements.define('service-profile', ServiceProfile);