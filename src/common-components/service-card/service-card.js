import { LitElement, html, css } from 'lit-element';

import '@material/mwc-icon';
import '@material/mwc-icon-button';

class ServiceCard extends LitElement
{
    constructor()
    {
        super();
        this.serviceImage = {
            "dCache" : "dcache",
            "OpenStack" : "openstack",
            "Mattermost" : "mattermost",
            "Zammad" : "zammad",
            "GitLab" : "gitlab",
            "JupyterHub" : "jupyterhub",
            "SonarQube" : "sonarqube",
            "Nextcloud" : "nextcloud",
            "bwSync&Share" : "nextcloud",
            "B2Share" : "b2share"
        };
        this.test = "A";
        this.abbreviation = "";
        this.img = "";
        this.software = "";
    }
    static get properties()
    {
        return {
            service: {
                type: Object
            },
            serviceImage: {
                type: Object
            },
            abbreviation: {
                type: String
            },
            img: {
                type: String
            },
            software: {
                type: String
            }
        };
    }
    static get styles()
    {
        return css`
            :host {
                display: inline-block;
            }
            * {
                box-sizing: border-box;
            }
            .card {
                width: 100%;
                height: 229px;
                box-shadow: none;
                transition: .3s transform cubic-bezier(.155,1.105,.295,1.12),
                            .3s box-shadow,
                            .3s -webkit-transform cubic-bezier(.155,1.105,.295,1.12);
                border-radius: 4px;
                border: 1px solid #ccc;
                background-color: #fff;
            }
            .card .top {
                width: 100%;
                height: 180px;
                padding: 14px 80px 10px 36px;
                background-repeat: no-repeat;
                background-position: right;
                background-size: 80px;
            }
            .card:hover {
                box-shadow: 0 10px 20px rgba(0,0,0,.12), 0 4px 8px rgba(0,0,0,.06);
            }
            .top img {
              top: 20px;
              right: 15px;
              max-height: 120px;
            }
            h2 {
                font-weight: 600;
                margin: 0;
                color: #035ba0;
                cursor: pointer;
            }
            .software {
                display: flex;
                align-items: center;
                margin-top: 5px;
            }
            .software img {
                max-width: 40px;
                max-height: 40px;
            }
            .software span {
                padding-left: 10px;
            }
            .description {
                display: -webkit-box;
                max-height: 70px;
                max-width: 330px;
                width: auto;
                overflow: hidden;
                text-overflow: ellipsis;
                font-size: 0.7em;
                padding: 8px 5px 0px 0px;
                -webkit-line-clamp: 4;
                -webkit-box-orient: vertical;
            }
            .card .bottom {
                display: flex;
                align-items: center;
                padding-left: 20px;
                padding-right: 0;
                height: 44px;
                width: 100%;
                border-top: 1px solid #ccc;
            }
            .bottom span {
                font-weight: 400;
                margin-right: 4px;
                font-size: 0.8em;
                font-style: italic;
            }
            .bottom img {
                max-width: 70px;
                max-height: 35px;
            }
            .bottom .use-button {
                flex: 1 1 auto;
                text-align: end;
            }
            .bottom a {
                text-decoration: none;
                color: #009688;
                padding: 5px;
                border-radius: 4px;
                font-size: 0.82em;
                font-weight: 200;
            }
            a mwc-icon {
                --mdc-icon-size: 0.82em;
            }
            .bottom a:hover {
                background-color: #e5f4f3;
            }
            .bottom mwc-icon-button {
                color: #333;
            }
        `;
    }
    render()
    {
        const noPoster = "data:image/svg+xml,%3Csvg%20width%3D%22344%22%20height" +
            "%3D%22194%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%" +
            "22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22" +
            "%3E%3Cdefs%3E%3Cpath%20id%3D%22a%22%20d%3D%22M-1%200h344v194H-1z%" +
            "22%2F%3E%3C%2Fdefs%3E%3Cg%20transform%3D%22translate(1)%22%20fill" +
            "%3D%22none%22%20fill-rule%3D%22evenodd%22%3E%3Cmask%20id%3D%22b%22" +
            "%20fill%3D%22%23fff%22%3E%3Cuse%20xlink%3Ahref%3D%22%23a%22%2F%3E%3" +
            "C%2Fmask%3E%3Cuse%20fill%3D%22%23BDBDBD%22%20xlink%3Ahref%3D%22%23a" +
            "%22%2F%3E%3Cg%20mask%3D%22url(%23b)%22%3E%3Cpath%20d%3D%22M173.65%206" +
            "9.238L198.138%2027%20248%20112.878h-49.3c.008.348.011.697.011%201.0" +
            "46%200%2028.915-23.44%2052.356-52.355%2052.356C117.44%20166.28%2094%" +
            "20142.84%2094%20113.924c0-28.915%2023.44-52.355%2052.356-52.355%2010%" +
            "200%2019.347%202.804%2027.294%207.669zm0%200l-25.3%2043.64h50.35c-" +
            ".361-18.478-10.296-34.61-25.05-43.64z%22%20fill%3D%22%23757575%22%2" +
            "F%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E";

        return html`
            <div class="card">
                <div class="top">
                    <h2 class="title" @click="${this._serviceDetailsDispatch}">
                        ${this.service['name'] == null ?
                            `No name` : this.service.name}
                    </h2>
                    <div class="software">
                        <img src="data:image/svg+xml;base64,${this.img}"/>
                        <span>${this.software}</span>
                    </div>
                    <div class="description">
                        ${this.service.summary ?
                            this._returnString(this.service.summary):
                            this._returnString(this.service.description)}
                    </div>
                </div>
                <div class="bottom">
                    <span>by:</span>
                    <img src=${this.service['serviceProviders'] == null ? noPoster :
                        `./media/centres/${this.abbreviation.toLowerCase()}.svg`} />
                    <div class="use-button">
                        <a target="_blank" rel="noopener noreferrer"
                            href="${this.service.entryPoint}"><mwc-icon>arrow_forward</mwc-icon> Go to service</a>
                    </div>
                    <mwc-icon-button icon="more_vert" @click="${this._serviceDetailsDispatch}"></mwc-icon-button>
                </div>
            </div>
        `;
    }

    firstUpdated()
    {
        // fetch some additional ressources
        super.firstUpdated();
        const headers = new Headers({
            "Accept": "application/json"
        });
        fetch(`${window.location.origin}/api/v0/images/${this.service.logoId}`, {headers: headers})
        .then((response) => {
            if (response.status !== 200) {
                throw new Error(`Looks like there was a problem. Status Code: ${response.status}`);
            }
            return response.json();
        })
        .then(data => {
            this.img = data.image;
        });
        fetch(`${window.location.origin}/api/v0/organizations/${this.service.serviceProviders[0]}`, {headers: headers})
        .then((response) => {
            if (response.status !== 200) {
                throw new Error(`Looks like there was a problem. Status Code: ${response.status}`);
            }
            return response.json();
        })
        .then(data => {
            this.abbreviation = data.abbreviation;
        });
        fetch(`${window.location.origin}/api/v0/software/${this.service.softwareList[0]}`, {headers: headers})
        .then((response) => {
            if (response.status !== 200) {
                throw new Error(`Looks like there was a problem. Status Code: ${response.status}`);
            }
            return response.json();
        })
        .then(data => {
            this.software = data.name;
        });
    }

    _serviceDetailsDispatch()
    {
        this.dispatchEvent(new CustomEvent('helmholtz-cloud-service-metadata', {
            detail: {message: this.service}, bubbles: true, composed: true}))
    }
    _returnString(str)
    {
        return document.createRange().createContextualFragment(`${ str }`);
    }
}
customElements.define('service-card', ServiceCard);