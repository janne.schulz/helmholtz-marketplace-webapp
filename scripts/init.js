import Router from './Router.js';

function removeAllChildNodes(parent) {
    while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
    }
}
function initRouter() {
    const outlet = document.querySelector('#outlet');
    const router = new Router({ mode: 'hash', root: '/' });
    let child;
    router
        .add('/', async () => {
            await import('../src/helmholtz-marketplace-app').then(async () => {
                removeAllChildNodes(outlet);
                if (sessionStorage.getItem("auth_status") === "authenticated") {
                    await import('../src/views/services-view/services-view').then(() => {
                        child = document.createElement('helmholtz-marketplace-app');
                        child.appendChild(document.createElement('services-view'));
                    });
                } else {
                    await import('../src/views/landing-view/landing-view').then(() => {
                        child = document.createElement('helmholtz-marketplace-app');
                        child.appendChild(document.createElement('landing-view'));
                    });
                }
                outlet.appendChild(child);
            });
        })
        .add('/services', async () => {
            await import('../src/helmholtz-marketplace-app').then(async () => {
                await import('../src/views/services-view/services-view').then(() => {
                    child = document.createElement('helmholtz-marketplace-app');
                    child.appendChild(document.createElement('services-view'));
                    removeAllChildNodes(outlet);
                    outlet.appendChild(child);
                });
            });

        })
        .add('/forms', async () => {
            console.log("forms");
            await import('../src/helmholtz-marketplace-app').then(async () => {
                await import('../src/views/forms/forms-view').then(() => {
                    child = document.createElement('helmholtz-marketplace-app');
                    child.appendChild(document.createElement('forms-view'));
                    removeAllChildNodes(outlet);
                    outlet.appendChild(child);
                });
            });
        });
    if (window.location.pathname === '/')
        router.navigate(
            window.location.hash === ''
                ? '/'
                : window.location.hash.substring(1)
        );
}
window.addEventListener('load', () => {
    initRouter();
});

window.addEventListener('authentication-status-checker', () => {
    if (sessionStorage.getItem('auth_status') === 'authenticated') {
        if (!sessionStorage.getItem('reload_1')) {
            sessionStorage.setItem('reload_1', 'ok');
            window.location.reload();
        } else {
            sessionStorage.removeItem('reload_1');
        }
    }
    document.getElementById('loading-overlay').hidden = true;
    document.body.classList.remove('hidden-overflow');
});