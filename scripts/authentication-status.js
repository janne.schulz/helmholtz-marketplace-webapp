(()=>{
    function triggerEvent() {
        const aa = setInterval(() => {
            if (document.readyState === "complete") {
                clearInterval(aa);
                window.dispatchEvent(new CustomEvent('authentication-status-checker', { detail: {isLoading: false}}));
            }
        }, 100)
    }

    fetch(`${window.location.origin}/tokens`, {
        headers: {
            "Content-Type": "Application/json",
            "Accept": "Application/json"
        }
    }).then(response => {
        if (response.status !== 200) {
            throw new Error(`Looks like there was a problem. Status Code: ${response.status}`);
        }
        return response.json();
    }).then(credential => {
        sessionStorage.setItem("auth_status", "authenticated");
        sessionStorage.setItem("access_token", credential["access_token"]);
        triggerEvent();
    }).catch(e => {
        console.log(e);
        if (e.message.includes("401") && sessionStorage.getItem("auth_status") === "authenticated") {
            sessionStorage.clear();
        }
        triggerEvent();
    })
})()
